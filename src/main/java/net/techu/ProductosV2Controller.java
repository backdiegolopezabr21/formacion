package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class ProductosV2Controller {

    private ArrayList<Producto> listaProductos = null;
    public ProductosV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1", 27.35));
        listaProductos.add(new Producto(2, "PR2", 18.33));
    }

    /*Set lista de productos*/
    //@RequestMapping(value ="/V2productos", method = RequestMethod.GET, produces = "application/json")
    @GetMapping(value ="/V2productos", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerlistado(){
        System.out.println("Estoy en obtener");
        return ResponseEntity.ok(listaProductos);
    }

    //@RequestMapping(value = "productos/{id}", method = RequestMethod.GET)
    @GetMapping("/V2productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable("id") int id) {
        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try {
            resultado = listaProductos.get(id);
            respuesta = ResponseEntity.ok(resultado);
        } catch (Exception ex) {
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }


    /*Add nuevo producto*/
    //@RequestMapping(value = "/V2productos", method = RequestMethod.POST, produces = "applicarion/json")
    @PostMapping("/V2productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoNuevo){
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(99, nombre, 100.5));
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    //@RequestMapping(value = "/V2productos/{nom}/{cat}", method = RequestMethod.POST, produces = "applicarion/json")
    @PostMapping(value = "/V2productos/{nom}/{cat}", produces = "applicarion/json")
    public ResponseEntity addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria){
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Producto(3, "PR3", 19.33));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/V2productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody Producto cambios){
        try{
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("Voy a modificar el producto");
            System.out.println("Nombre Actual " + productoAModificar.getNombre());
            System.out.println("Nombre Nuevo " + cambios.getNombre());
            System.out.println("Precio Actual " + productoAModificar.getPrecio());
            System.out.println("Precio Nuevo " + cambios.getPrecio());
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(id, productoAModificar);
            return new ResponseEntity<>("Producto modificado correctamente", HttpStatus.NO_CONTENT);
        }catch (Exception ex){
            return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/V2productos/")
    public ResponseEntity<String> updateProductoSinId(@RequestBody Producto cambios){
        try{
            Producto productoAModificar = listaProductos.get(cambios.getId());
            System.out.println("Voy a modificar el producto");
            System.out.println("Nombre Actual " + productoAModificar.getNombre());
            System.out.println("Nombre Nuevo " + cambios.getNombre());
            System.out.println("Precio Actual " + productoAModificar.getPrecio());
            System.out.println("Precio Nuevo " + cambios.getPrecio());
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(cambios.getId(), productoAModificar);
            return new ResponseEntity<>("Producto modificado correctamente", HttpStatus.NO_CONTENT);
        }catch (Exception ex){
            return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping("/V2productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id){
        try{
            System.out.println("Voy a eliminar el producto de Id "+id);
            listaProductos.remove(id-1);
            return new ResponseEntity<>("Eliminado correctamente", HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/V2productos")
    public ResponseEntity<String> deleteAllProducts(){
            System.out.println("Estoy en eliminar todos");
            listaProductos.clear();
            return new ResponseEntity<>("Eliminado correctamente", HttpStatus.OK);
    }

}